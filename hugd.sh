#/bin/bash
# ----------------------------------------------------------------------------
# "THE HUG-WARE LICENSE" (Revision 42):
# <mojo _ AT_ couak _DOT _ net> wrote this file.
# As long as you retain this notice you can do whatever you want with this
# stuff. If we meet some day, and you think this stuff is worth it, you can
# kindly ask to hug me.   Jérôme.
# ----------------------------------------------------------------------------

# YOU !
# If you are reading a file with "hug" inside, I assume your are ok
# to read and to adjust the following sentences to your hug needs
BUCKET_OF_LOVE=(
    "You derserve it, here is a hug"
    "Bryan told me it is OK to virtual hug him"
    "Would you allow me to eventually send you some virtual hugs ?"
    "\\o/ ... Ops, I did it again"
    "hug \\o/"
    "o// \\\\o"
    "Hive Five"
    "If you eventually need it, I can give you a virtual hug: \\o/"
    "If you are looking at your logs, we are probably having a bad time, here is a hug: \\o/"
    "\\o/"
    "That's too bad ... °hug°"
    "It's dangerous to go alone, take this hug: \\o/"
    "If you explicitely want me to stop sending virtual hug to your logs, you may just want to stop me... because I _AM_ H.U.G.s"
)
WELCOME_HUG="Hug Daemon started to spread humanity and kindness"
EXIT_HUG="Here is my last virtual hug before ending my mission: \\o/"

function log {
    local msg="${@:1}"
    logger -p local1.notice -t hugd $msg
}

function random_message {
    local love_bucket_size=${#BUCKET_OF_LOVE[@]}
    local random_love=$(( RANDOM % love_bucket_size ))
    log ${BUCKET_OF_LOVE[$random_love]}
}

function random_sleep {
    # min: 1h max: 5h
    sleep $(( ( RANDOM % 5 + 1 ) * 60 * 60 ))
}

trap "log $EXIT_HUG; exit" TERM EXIT
log $WELCOME_HUG
while ((42)); do
    random_message
    random_sleep
done
