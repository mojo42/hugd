# Hug Daemon

Yup, that's a daemon that anyone would love to run on his machine.

This will send every few hours some virtual hugs to your logs so it will give you some support in bad times, particulary when you have to look at your logs which tells you so much bad things happening to your system.

# How to install hugd

Like in [this example](https://www.ubuntudoc.com/how-to-create-new-service-with-systemd/)
